# sus

## Super User Set

Be the imposter on YOUR unix system!! Run commands *as* root while *not* being root ???

## Usage

sus <command> <args>

## Config

Add usernames who should be allowed super user access, line by line, in /etc/sussyconf

Example:
```
myuser
anotheruser
```

pain
