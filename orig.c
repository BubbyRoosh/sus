#include <stdio.h> // Other headers like stdlib.h and string.h could be omitted. Removing these 2 would cause segmentation faults, however.
#include <unistd.h>
line[1024], *fp; // Funky initialization.
int main(int argc, char **argv) {
    strcat((argv[0] = getlogin()), "\n"); // Set argv[0] to the name of the user running it and append \n to the end.
    if(argv[1] && (fp = fopen("/etc/sussyconf", "r")) != NULL) while(fgets(line, 20, fp)) // Check if there's at least 1 argument and loop each line if the config file could be opened.
        if(!strcmp(line, argv[0]) && setuid(0) != -1 && setgid(0) != -1 && seteuid(0) != -1 && setegid(0) != -1) return execvp(argv[1], argv + 1);
} //    ^^ If the line equals the user's name and the program could set the uid/gid/euid/egid, run the program with the rest of the arguments and return that value.
