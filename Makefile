DESTDIR=/usr
PREFIX=/local

UNIXDIR!=./getunixdir

sus: sus.c
	$(CC) sus.c -o sus -pedantic -static -Oz

clean:
	rm ./sus

install: sus
	mkdir -p ${DESTDIR}${PREFIX}/bin
	install -o root -m 4555 sus ${DESTDIR}${PREFIX}/bin/sus
	install _sus ${UNIXDIR}/_sus

uninstall:
	rm ${DESTDIR}${PREFIX}/bin/sus
	rm ${UNIXDIR}/_sus
